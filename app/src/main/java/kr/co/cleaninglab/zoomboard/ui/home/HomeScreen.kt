package kr.co.cleaninglab.zoomboard.ui.home

import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.eugene.logcatlib.Logcat
import kotlinx.coroutines.flow.MutableStateFlow
import kr.co.cleaninglab.zoomboard.R
import kr.co.cleaninglab.zoomboard.ui.Actions
import kr.co.cleaninglab.zoomboard.ui.Destinations
import kr.co.cleaninglab.zoomboard.ui.common.MyTopAppBar
import kr.co.cleaninglab.zoomboard.ui.splash.NextAction
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeLight
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain
import kr.co.cleaninglab.zoomboard.ui.theme.ZoomBoardTheme

enum class LoginStep {
    TOKEN,
    NAME,
    NONE
}

@Composable
fun HomeScreen(
    viewModel: HomeViewModel = hiltViewModel(),
    actions: Actions
) {
    val navController = rememberNavController()

    val loginStepState = MutableStateFlow<LoginStep?>(null)
    val loginStepCollect = loginStepState.collectAsState()
    loginStepCollect.value?.let {
        when (it) {
            LoginStep.NAME -> actions.goName(NextAction.Create)
            LoginStep.TOKEN -> actions.goLogin
            LoginStep.NONE -> actions.goCreate
        }
    }

    ZoomBoardTheme(darkTheme = false) {
        Surface(color = MaterialTheme.colors.background) {
            Scaffold(
                topBar = { MyTopAppBar("Zoom Board", false) },
                bottomBar = {
                    BottomBar(navController = navController)
                },
                floatingActionButton = {
                    FloatingActionButton(
                        onClick = { checkLoginAndCreate(viewModel, loginStepState) },
                        backgroundColor = OrangeMain) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_plus),
                            contentDescription = null
                        )
                    }
                },
            ) {
                BottomTabNavGraph(
                    navController = navController,
                    actions = actions
                )
            }
        }
    }
}

private fun checkLoginAndCreate(
    viewModel: HomeViewModel,
    loginStep: MutableStateFlow<LoginStep?>
) {
    when {
        viewModel.nameFlow.value.isBlank() -> {
            loginStep.value = LoginStep.NAME
        }
        viewModel.tokenFlow.value.isBlank() -> {
            loginStep.value = LoginStep.TOKEN
        }
        else -> {
            loginStep.value = LoginStep.NONE
        }
    }
}

sealed class Screen(val route: String, @StringRes val resourceId: Int, val icon: ImageVector) {
    object HomeList : Screen(Destinations.HomeList, R.string.home, Icons.Filled.Home)
    object MyClass : Screen(Destinations.MyClass, R.string.my_class, Icons.Filled.Favorite)
    object Menu : Screen(Destinations.Menu, R.string.menu, Icons.Filled.Menu)
}

val items = listOf(
    Screen.HomeList,
    Screen.MyClass,
    Screen.Menu
)

@Composable
private fun BottomBar(
    navController: NavController
) {
    BottomNavigation(
        backgroundColor = OrangeLight
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentDestination = navBackStackEntry?.destination
        items.forEach { screen ->
            BottomNavigationItem(
                icon = { Icon(screen.icon, contentDescription = null) },
                label = {
                    Text(
                        text = stringResource(screen.resourceId),
                        color = Color.Black
                    )
                },
                selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
                onClick = {
                    navController.navigate(screen.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                }
            )
        }
    }
}

@Composable
private fun BottomTabNavGraph(
    navController: NavHostController,
    actions: Actions
) {
    NavHost(navController, startDestination = Destinations.HomeList) {
        composable(Destinations.HomeList) {
            HomeListScreen(actions = actions)
        }
        composable(Destinations.MyClass) {
            MyClassScreen()
        }
        composable(Destinations.Menu) {
            MenuScreen()
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HomePreview() {
    val navController = rememberNavController()
    HomeScreen(actions = Actions(navController))
}