package kr.co.cleaninglab.zoomboard.net

import kr.co.cleaninglab.zoomboard.model.*
import okhttp3.MultipartBody
import retrofit2.http.*

interface Api {

    @FormUrlEncoded
    @POST("/login/custom")
    suspend fun login(
        @Field("id") id: String,
        @Field("password") password: String
    ): Response<Login>

    @GET("/zoomboard/list")
    suspend fun getList(): Response<List<ZoomBoard>>

    @GET("/zoomboard/{id}")
    suspend fun getDetail(
        @Path("id") id: Int
    ): Response<ZoomBoard>

    @POST("/user/create")
    suspend fun createUser(
        @Body loginRequest: LoginRequest
    ): Response<String>

    @GET("/user/profile")
    suspend fun getProfile(): Response<Author>

    @PUT("/user/profile")
    suspend fun setProfile(
        @Body profileRequest: ProfileRequest
    ): Response<String>

    @Multipart
    @POST("/zoomboard/12/upload-image")
    suspend fun putBitmap(
        @Part image: MultipartBody.Part
    ): Response<String>

    @POST("/zoomboard/create")
    suspend fun create(
        @Body createRequest: CreateRequest
    ): Response<String>
}