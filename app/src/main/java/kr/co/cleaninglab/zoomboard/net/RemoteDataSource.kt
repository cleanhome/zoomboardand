package kr.co.cleaninglab.zoomboard.net

import android.graphics.Bitmap
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import kr.co.cleaninglab.zoomboard.model.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSource @Inject constructor(
    private val dispatchers: CoroutineDispatcher,
    private val api: Api
) {

    suspend fun login(id: String, password: String): ResultWrapper<Login> {
        return doNetworking(dispatchers, suspend {
            api.login(id, password)
        })
    }

    suspend fun login(googleToken: String): ResultWrapper<String> {
        return doNetworking(dispatchers, suspend {
            api.createUser(LoginRequest("GOOGLE", googleToken))
        })
    }

    suspend fun getList(): ResultWrapper<List<ZoomBoard>> {
        return doNetworking(dispatchers, suspend {
            api.getList()
        })
    }

    suspend fun putBitmap(file: File): ResultWrapper<String> {
        val requestBody: RequestBody = file.asRequestBody("image/*".toMediaTypeOrNull())
        val body = MultipartBody.Part.createFormData("file", file.absolutePath, requestBody)

        return doNetworking(dispatchers, suspend {
            api.putBitmap(body)
        })
    }

    suspend fun create(createRequest: CreateRequest): ResultWrapper<String> {
        return doNetworking(dispatchers, suspend {
            api.create(createRequest = createRequest)
        })
    }

    suspend fun getDetail(id: Int): ResultWrapper<ZoomBoard> {
        return doNetworking(dispatchers, suspend {
            api.getDetail(id)
        })
    }

    suspend fun getProfile(): ResultWrapper<Author> {
        return doNetworking(dispatchers, suspend {
            api.getProfile()
        })
    }

    suspend fun setProfile(profileRequest: ProfileRequest): ResultWrapper<String> {
        return doNetworking(dispatchers, suspend {
            api.setProfile(profileRequest)
        })
    }

    private suspend fun <T> doNetworking(
        dispatchers: CoroutineDispatcher,
        f: suspend () -> Response<T>,
        handleCommonError: Boolean = true
    ): ResultWrapper<T> {
        return withContext(dispatchers) {
            try {
                val value = checkResult(f())
                ResultWrapper.Success(value)
            } catch (t: Throwable) {
                // TODO handling of exceptions
                ResultWrapper.Error(type = ResultError.COMMON, t)
            }
        }
    }

    private fun <T> checkResult(response: Response<T>): T {
        if (response.code == OK) {
            val value = response.result
            return value!!
        } else {
            throw ResponseException.of(response)
        }
    }
}