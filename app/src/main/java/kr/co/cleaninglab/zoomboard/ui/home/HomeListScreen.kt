package kr.co.cleaninglab.zoomboard.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import com.eugene.logcatlib.Logcat
import kr.co.cleaninglab.zoomboard.model.Author
import kr.co.cleaninglab.zoomboard.model.ZoomBoard
import kr.co.cleaninglab.zoomboard.ui.Actions
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeDark

@Composable
fun HomeListScreen(
    viewModel: HomeListViewModel = hiltViewModel(),
    actions: Actions
) {
    val listState: MutableState<List<ZoomBoard>> = remember { mutableStateOf(emptyList()) }
    val resultHomeList = viewModel.homeListFlow.collectAsState()
    listState.value = resultHomeList.value

    Logcat.debug("--- projects : $listState")

    LazyColumn(
        modifier = Modifier
            .fillMaxSize(),
        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
    ) {
        itemsIndexed(
            items = listState.value,
            itemContent = { _, item ->
                BoardItem(
                    board = item,
                    itemClick = {
                        actions.goDetail(item.id.toString())
                    }
                )
            }
        )
    }
}

@Composable
private fun BoardItem(
    board: ZoomBoard,
    itemClick: (ZoomBoard) -> Unit
) {
    ConstraintLayout(modifier = Modifier
        .padding(10.dp)
        .clickable { itemClick(board) }
    ) {
        val (image, profileImage, title, desc) = createRefs()
        Image(
            modifier = Modifier
                .constrainAs(image) {
                    top.linkTo(parent.top)
                }
                .fillMaxWidth()
                .height(100.dp)
                .clip(RoundedCornerShape(10.dp)),
            painter = painterResource(kr.co.cleaninglab.zoomboard.R.drawable.brunch_image),
            contentDescription = null,
            contentScale = ContentScale.None
        )
        Image(
            modifier = Modifier
                .padding(top = 10.dp)
                .constrainAs(profileImage) {
                    top.linkTo(image.bottom)
                    start.linkTo(parent.start)
                }
                .size(40.dp)
                .clip(RoundedCornerShape(25.dp)),
            painter = painterResource(kr.co.cleaninglab.zoomboard.R.drawable.image_1),
            contentDescription = null,
            contentScale = ContentScale.None
        )
        Text(text = board.title,
            modifier = Modifier
                .padding(top = 10.dp)
                .wrapContentSize()
                .constrainAs(title) {
                    top.linkTo(image.bottom)
                    start.linkTo(profileImage.end)
                }
        )
        Text(text = board.description,
            modifier = Modifier
                .wrapContentSize()
                .constrainAs(desc) {
                    top.linkTo(title.bottom)
                    start.linkTo(profileImage.end)
                })
    }
}

@Preview(showBackground = true)
@Composable
fun BoardItemPreview() {
    val author = Author("제주 감귤 아저씨", null)
    BoardItem(
        ZoomBoard(0, author, "테스트1234", "테스트테스트", 0),
        {}
    )
}