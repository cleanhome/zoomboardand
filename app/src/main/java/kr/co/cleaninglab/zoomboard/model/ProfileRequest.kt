package kr.co.cleaninglab.zoomboard.model

data class ProfileRequest(
    val name: String
)