package kr.co.cleaninglab.zoomboard.local

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class SharedPreferences @Inject constructor(
    private val preferences: Preferences
) {
    fun getToken(): Flow<String> {
        return preferences.getString(KEY_TOKEN, "N8jN6G9rW5R9h0DF7dqup8EglrZPDzZb7BAPmh2lQyk=")
    }

    suspend fun setToken(token: String) {
        preferences.set(KEY_TOKEN, token)
    }

    fun getName(): Flow<String> {
        return preferences.getString(KEY_NAME, "")
    }

    suspend fun setName(name: String) {
        preferences.set(KEY_NAME, name)
    }

    var tokenByBlocking: String = runBlocking {
        preferences.getString(KEY_TOKEN, "N8jN6G9rW5R9h0DF7dqup8EglrZPDzZb7BAPmh2lQyk=").first()
    }
}