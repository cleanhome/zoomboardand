package kr.co.cleaninglab.zoomboard.local

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

const val KEY_TOKEN = "token"
const val KEY_NAME = "name"

@Singleton
class Preferences @Inject constructor(@ApplicationContext val context: Context) {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = DATA_STORE_NAME)

    suspend fun set(key: String, value: String) {
        val stringKey = stringPreferencesKey(key)
        context.dataStore.edit { preferences ->
            preferences[stringKey] = value
        }
    }

    suspend fun set(key: String, value: Long) {
        val longKey = longPreferencesKey(key)
        context.dataStore.edit { preferences ->
            preferences[longKey] = value
        }
    }

    fun getString(key: String, default: String): Flow<String> =
        context.dataStore.data.map { preferences ->
            val stringKey = stringPreferencesKey(key)
            preferences[stringKey] ?: default
        }

    fun getStringOrNull(key: String): Flow<String?> =
        context.dataStore.data.map { preferences ->
            val stringKey = stringPreferencesKey(key)
            preferences[stringKey]
        }

    fun getLong(
        key: String,
        default: Long
    ): Flow<Long> = context.dataStore.data.map { preferences ->
        val longKey = longPreferencesKey(key)
        preferences[longKey] ?: default
    }

    fun getLongOrNull(key: String): Flow<Long?> =
        context.dataStore.data.map { preferences ->
            val longKey = longPreferencesKey(key)
            preferences[longKey]
        }

    companion object {
        const val DATA_STORE_NAME = "preferences"
    }
}