package kr.co.cleaninglab.zoomboard.ui

import androidx.navigation.NavHostController
import androidx.navigation.navOptions
import kr.co.cleaninglab.zoomboard.ui.Destinations.Create
import kr.co.cleaninglab.zoomboard.ui.Destinations.Detail
import kr.co.cleaninglab.zoomboard.ui.Destinations.Home
import kr.co.cleaninglab.zoomboard.ui.Destinations.Name
import kr.co.cleaninglab.zoomboard.ui.Destinations.Splash

object Destinations {
    const val Splash = "splash"
    const val Name = "Name"
    const val Home = "home"
    const val Create = "create"
    const val HomeList = "home_list"
    const val MyClass = "my_class"
    const val Menu = "menu"
    const val Detail = "detail"

    object NextArgs {
        const val nextValue = "nextValue"
    }

    object DetailArgs {
        const val detailId = "detailId"
    }
}

class Actions(navController: NavHostController) {
    val goHome: () -> Unit = {
        navController.navigate(Home, navOptions {
                popUpTo(Splash, { inclusive = true })
            }
        )
    }
    val goCreate: () -> Unit = {
        navController.navigate(Create)
    }
    val goLogin:() -> Unit = {
        navController.navigate(Splash)
    }
    val goName:(String) -> Unit = { nextAction ->
        navController.navigate("$Name/$nextAction")
    }
    val goDetail:(String) -> Unit = { id ->
        navController.navigate("$Detail/$id")
    }
    val navigateUp: () -> Unit = {
        navController.popBackStack()
    }
}