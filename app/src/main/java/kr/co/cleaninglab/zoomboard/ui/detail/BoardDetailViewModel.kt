package kr.co.cleaninglab.zoomboard.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kr.co.cleaninglab.zoomboard.local.HomeListRepository
import kr.co.cleaninglab.zoomboard.model.emptyBoard
import javax.inject.Inject

@HiltViewModel
class BoardDetailViewModel @Inject constructor(
    private val repository: HomeListRepository
) : ViewModel() {

    val detailFlow = MutableStateFlow(emptyBoard())

    fun getDetail(id: Int) = viewModelScope.launch {
        detailFlow.value = repository.getDetail(id)
    }
}