package kr.co.cleaninglab.zoomboard.ui.splash

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.eugene.logcatlib.Logcat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.transform
import kr.co.cleaninglab.zoomboard.R
import javax.inject.Singleton

const val REQUEST_GOOGLE_SIGN_IN_ONE_TAP = 2

object LoginManager {

    private var mGoogleSignInClient: GoogleSignInClient? = null

    val googleIdTokenFlow = MutableStateFlow("")

    fun initGoogleLogin(activity: Activity) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity.getString(R.string.google_web_application_id))
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso)
        val signInIntent = mGoogleSignInClient!!.signInIntent
        activity.startActivityForResult(signInIntent, REQUEST_GOOGLE_SIGN_IN_ONE_TAP)
    }

    fun handleGoogleSignInResult(data: Intent?) {
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            task.getResult(ApiException::class.java)?.let { account ->
                account.idToken?.let { token ->
                    googleIdTokenFlow.value = token
                } ?: kotlin.run {
                    Logcat.error(Throwable("check the google login"))
                }
            }

        } catch (e: ApiException) {
            Logcat.error(Throwable(e))
        }
    }
}