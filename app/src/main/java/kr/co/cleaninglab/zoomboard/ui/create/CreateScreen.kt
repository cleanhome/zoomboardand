package kr.co.cleaninglab.zoomboard.ui.create

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.hilt.navigation.compose.hiltViewModel
import kr.co.cleaninglab.zoomboard.R
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeDark
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain
import kr.co.cleaninglab.zoomboard.ui.theme.ZoomBoardTheme
import kr.co.cleaninglab.zoomboard.ui.theme.textFieldBackground
import kr.co.cleaninglab.zoomboard.util.*
import java.util.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.asImageBitmap
import androidx.navigation.compose.rememberNavController
import com.eugene.logcatlib.Logcat
import kr.co.cleaninglab.zoomboard.model.CreateRequest
import kr.co.cleaninglab.zoomboard.ui.Actions
import kr.co.cleaninglab.zoomboard.ui.common.*

@Composable
fun CreateScreen(
    viewModel: CreateViewModel = hiltViewModel(),
    actions: Actions
) {
    ZoomBoardTheme(darkTheme = false) {
        val context = LocalContext.current

        val openCountDialog = remember { mutableStateOf(false)  }
        val titleValue = remember { mutableStateOf(TextFieldValue()) }
        val descValue = remember { mutableStateOf(TextFieldValue()) }
        val dateValue = remember { mutableStateOf(Date().format("yyyy/MM/dd HH:mm")) }
        val countValue = remember { mutableStateOf("10") }
        val confirmDialog = remember { mutableStateOf(false) }

        val resultImagePath = viewModel.imagePathFlow.collectAsState()
        resultImagePath.value.let {
            if (it.isNotBlank()) {
                viewModel.create(
                    CreateRequest(
                        title = titleValue.value.text,
                        description = descValue.value.text,
                        maxCountUser = countValue.value.toInt(),
                        zoomLinkUrl = ""
                    )
                )
            }
        }

        val createSuccess = viewModel.createCompleteFlow.collectAsState()
        createSuccess.value.let {
            if (it.isNotBlank()) {
                confirmDialog.value = true
            }
        }

        val bitmap =  remember {
            mutableStateOf<Bitmap?>(null)
        }
        var imageUri by remember {
            mutableStateOf<Uri?>(null)
        }
        imageUri?.let {
            if (bitmap.value == null) {
                if (Build.VERSION.SDK_INT < 28) {
                    bitmap.value = MediaStore.Images
                        .Media.getBitmap(context.contentResolver, it)

                } else {
                    bitmap.value = ImageDecoder.decodeBitmap(
                        ImageDecoder.createSource(context.contentResolver, it)
                    ) { decoder: ImageDecoder, _: ImageDecoder.ImageInfo?, _: ImageDecoder.Source? ->
                        decoder.isMutableRequired = true
                        decoder.allocator = ImageDecoder.ALLOCATOR_SOFTWARE
                    }
                }
            }
        }
        val launcher = rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) { uri: Uri? ->
            Logcat.debug("--- uri : $uri")
            imageUri = uri
        }

        if (openCountDialog.value) {
            CountDialog(
                "참여인원",
                listOf("2", "3", "4", "5", "6", "7"),
                clickEvent = {
                    countValue.value = it
                },
                openState = openCountDialog
            )
        }

        if (confirmDialog.value) {
            CommonDialog(
                title = "등록되었습니다!",
                confirmClick = {
                confirmDialog.value = false
                    actions.navigateUp
            }) {
                confirmDialog.value = false
            }
        }

        Surface(color = MaterialTheme.colors.background) {
            Column(modifier = Modifier
                .fillMaxSize(),
            content = {
                MyTopAppBar(
                    title = "Create",
                    iconRes = R.drawable.ic_back_button,
                    iconVisible = true,
                    naviUpAction = actions.navigateUp
                )

                Column(modifier = Modifier
                    .fillMaxSize()
                    .padding(20.dp)
                    .verticalScroll(rememberScrollState()),
                content = {
                    InputText(title = "클래스 타이틀",
                        hintText = "함께 해요!",
                        inputValue = titleValue)

                    InputText(title = "설명",
                        hintText = "사람들과 공유하고 싶은 내용을 자세하게 적어주세요~",
                        inputValue = descValue,
                        singleLine = false
                    )

                    TextWithAction(title = "시작시간",
                        valueText = dateValue.value,
                        clickAction = {
                            showDateDialog(context, dateValue)
                        }
                    )

                    TextWithAction(
                        title = "참여인원",
                        valueText = countValue.value.toString(),
                        clickAction = {
                            openCountDialog.value = true
                        }
                    )

                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .height(160.dp)
                        .clickable {
                            launcher.launch("image/*")
                        }
                        .background(textFieldBackground, shape = RoundedCornerShape(10.dp)),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = "대표사진 입력",
                        )
                        bitmap.value?.let {  btm ->
                            Image(bitmap = btm.asImageBitmap(),
                                contentDescription =null,
                                modifier = Modifier.size(160.dp))
                        }
                    }

                    Button(modifier = Modifier
                        .padding(top = 20.dp)
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .background(color = OrangeMain, shape = RoundedCornerShape(15.dp)),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = OrangeMain,
                            contentColor = Color.Black
                        ),
                        onClick = {
                            bitmap.value?.let {
                                viewModel.putBitmap(it)
                            }
                        }
                    ) {
                        Text(text = "만들기",
                            modifier = Modifier.padding(15.dp),
                            color = Color.Black
                        )
                    }
                })
            })
        }
    }
}

private fun showDateDialog(context: Context, stateValue: MutableState<String>) {
    DatePickerDialog(
        context,
        { _, year, month, dayOfMonth ->
            val d = getDate(year, month, dayOfMonth)
            stateValue.value = d.format("yyyy/MM/dd")
            showTimeDialog(context, stateValue)
        },
        currentYear(), currentMonth(), currentDay()
    ).show()
}

private fun showTimeDialog(context: Context, stateValue: MutableState<String>) {
    TimePickerDialog(
        context,
        { _, hourOfDay, minute ->
            val d = getTime(hourOfDay, minute)
            stateValue.value = "${stateValue.value} ${d.format("HH:mm")}"
        },
        currentHour(), currentMinute(), false
    ).show()
}

@Preview(showBackground = true)
@Composable
fun CreatePreview() {
    val navController = rememberNavController()
    CreateScreen(actions = Actions(navController))
}