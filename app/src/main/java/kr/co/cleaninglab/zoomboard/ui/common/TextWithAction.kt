package kr.co.cleaninglab.zoomboard.ui.common

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kr.co.cleaninglab.zoomboard.ui.theme.textFieldBackground

@Composable
fun TextWithAction(
    title: String,
    valueText: String,
    bottomMargin: Int = 26,
    clickAction: () -> Unit
) {
    Column() {
        Text(text = title)
        Text(
            text = valueText,
            modifier = Modifier
                .padding(top = 10.dp, bottom = bottomMargin.dp)
                .fillMaxWidth()
                .background(textFieldBackground, shape = RoundedCornerShape(10.dp))
                .padding(14.dp)
                .clickable { clickAction() },
        )
    }
}

@Preview(showBackground = true)
@Composable
fun TextWithActionPreview() {
    TextWithAction(
        "클래스 제목",
        "상세텍스트",
        clickAction = {}
    )
}