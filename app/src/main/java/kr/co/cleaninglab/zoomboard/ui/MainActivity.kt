package kr.co.cleaninglab.zoomboard.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.eugene.logcatlib.Logcat
import dagger.hilt.android.AndroidEntryPoint
import kr.co.cleaninglab.zoomboard.ui.splash.LoginManager
import kr.co.cleaninglab.zoomboard.ui.splash.REQUEST_GOOGLE_SIGN_IN_ONE_TAP
import kr.co.cleaninglab.zoomboard.ui.theme.ZoomBoardTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ZoomBoardTheme {
                MainApp()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_GOOGLE_SIGN_IN_ONE_TAP) {
            LoginManager.handleGoogleSignInResult(data)
        }
    }
}