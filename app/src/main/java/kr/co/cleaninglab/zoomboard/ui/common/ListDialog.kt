package kr.co.cleaninglab.zoomboard.ui.common

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog

@Composable
fun CountDialog(
    title: String,
    list: List<String>,
    clickEvent: (String) -> Unit,
    openState: MutableState<Boolean>
) {
    Dialog(
        onDismissRequest = { openState.value = false },
        content = {
            Surface(
                modifier = Modifier
                    .width(300.dp)
                    .wrapContentHeight()
                    .background(Color.White, shape = RoundedCornerShape(10.dp))
                    .padding(20.dp)
            ) {
                Column {
                    Text(text = title)
                    LazyColumn(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(200.dp),
                        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
                    ) {
                        itemsIndexed(
                            items = list,
                            itemContent = { _, item ->
                                ListItem(
                                    item = item,
                                    itemClick = {
                                        openState.value = false
                                        clickEvent(item)
                                    }
                                )
                            }
                        )
                    }

                    Button(onClick = { openState.value = false }) {
                        Text(text = "Cancel", color = Color.Black)
                    }
                }
            }
        }
    )
}

@Composable
fun ListItem(
    item: String,
    itemClick: (String) -> Unit
) {
    Row {
        Column(modifier = Modifier
            .padding(10.dp)
            .clickable { itemClick(item) }
        ) {
            Text(text = item)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ListDialogPreview() {
    val list = listOf("1","2","3","4","5")
    val v = remember { mutableStateOf(false) }
    CountDialog(
        "인원수",
        list,
        {},
        v
    )
}