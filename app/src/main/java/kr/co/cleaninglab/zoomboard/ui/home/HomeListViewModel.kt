package kr.co.cleaninglab.zoomboard.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kr.co.cleaninglab.zoomboard.local.HomeListRepository
import kr.co.cleaninglab.zoomboard.model.ZoomBoard
import javax.inject.Inject

@HiltViewModel
class HomeListViewModel @Inject constructor(
    private val repository: HomeListRepository
): ViewModel() {

    val homeListFlow: StateFlow<List<ZoomBoard>> =
        homeList().stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(),
            emptyList()
        )

    fun homeList(): Flow<List<ZoomBoard>> = flow {
        //while (true) {
            val homeList = repository.getList()
            emit(homeList)
        //}
    }
}