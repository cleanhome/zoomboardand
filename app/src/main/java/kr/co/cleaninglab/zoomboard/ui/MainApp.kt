package kr.co.cleaninglab.zoomboard.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import kr.co.cleaninglab.zoomboard.ui.Destinations.Create
import kr.co.cleaninglab.zoomboard.ui.Destinations.Detail
import kr.co.cleaninglab.zoomboard.ui.Destinations.DetailArgs.detailId
import kr.co.cleaninglab.zoomboard.ui.Destinations.Home
import kr.co.cleaninglab.zoomboard.ui.Destinations.Name
import kr.co.cleaninglab.zoomboard.ui.Destinations.NextArgs.nextValue
import kr.co.cleaninglab.zoomboard.ui.Destinations.Splash
import kr.co.cleaninglab.zoomboard.ui.create.CreateScreen
import kr.co.cleaninglab.zoomboard.ui.detail.BoardDetailScreen
import kr.co.cleaninglab.zoomboard.ui.home.HomeScreen
import kr.co.cleaninglab.zoomboard.ui.splash.NameScreen
import kr.co.cleaninglab.zoomboard.ui.splash.SplashScreen
import kr.co.cleaninglab.zoomboard.ui.splash.SplashViewModel

@Composable
fun MainApp() {
    val navController = rememberNavController()
    val actions = remember(navController) { Actions(navController) }

    NavHost(navController, startDestination = Home) {
        composable(Splash) {
            SplashScreen(
                actions.goName,
                actions.goHome
            )
        }
        composable(Home) {
            HomeScreen(actions = actions)
        }
        composable(Create) {
            CreateScreen(
                actions = actions
            )
        }
        composable("$Name/{$nextValue}",
            arguments = listOf(navArgument(nextValue) { type = NavType.StringType })
        ) {
            NameScreen(
                nextAction = it.arguments?.getString(nextValue) ?: "",
                actions = actions
            )
        }
        composable("$Detail/{$detailId}",
            arguments = listOf(navArgument(detailId) { type = NavType.IntType })
        ) {
            BoardDetailScreen(
                id = it.arguments?.getInt(detailId) ?: 0,
                actions = actions
            )
        }
    }
}