package kr.co.cleaninglab.zoomboard.local

import com.eugene.logcatlib.Logcat
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kr.co.cleaninglab.zoomboard.model.ResultWrapper
import kr.co.cleaninglab.zoomboard.model.ZoomBoard
import kr.co.cleaninglab.zoomboard.model.emptyBoard
import kr.co.cleaninglab.zoomboard.net.RemoteDataSource
import javax.inject.Inject

class HomeListRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) {

    suspend fun getList(): List<ZoomBoard> {
        return try {
            when (val result = remoteDataSource.getList()) {
                is ResultWrapper.Success -> result.value
                is ResultWrapper.Error -> {
                    Logcat.error(result.throwable)
                    emptyList<ZoomBoard>()
                }
                else -> emptyList<ZoomBoard>()
            }
        } finally {
        }
    }

    suspend fun getDetail(id: Int): ZoomBoard {
        return try {
            when (val result = remoteDataSource.getDetail(id)) {
                is ResultWrapper.Success -> result.value
                is ResultWrapper.Error -> {
                    Logcat.error(result.throwable)
                    emptyBoard()
                }
                else -> emptyBoard()
            }
        } finally {
        }
    }
}