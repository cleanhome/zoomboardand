package kr.co.cleaninglab.zoomboard.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kr.co.cleaninglab.zoomboard.local.HomeRepository
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: HomeRepository
): ViewModel() {

    val tokenFlow: StateFlow<String> = repository.getToken().stateIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(),
        ""
    )

    val nameFlow: StateFlow<String> = repository.getName().stateIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(),
        ""
    )
}