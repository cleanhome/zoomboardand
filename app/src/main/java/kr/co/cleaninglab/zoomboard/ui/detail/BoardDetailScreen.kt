package kr.co.cleaninglab.zoomboard.ui.detail

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController
import com.eugene.logcatlib.Logcat
import kr.co.cleaninglab.zoomboard.R
import kr.co.cleaninglab.zoomboard.ui.Actions
import kr.co.cleaninglab.zoomboard.ui.common.MyTopAppBar
import kr.co.cleaninglab.zoomboard.ui.theme.ZoomBoardTheme
import java.lang.Float.min
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeDark
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain
import kr.co.cleaninglab.zoomboard.ui.theme.descTextColor
import me.onebone.toolbar.CollapsingToolbar
import me.onebone.toolbar.CollapsingToolbarScaffold
import me.onebone.toolbar.ScrollStrategy
import me.onebone.toolbar.rememberCollapsingToolbarScaffoldState

@Composable
fun BoardDetailScreen(
    id: Int,
    actions: Actions,
    viewModel: BoardDetailViewModel = hiltViewModel()
) {
    Logcat.debug("--- detailId : $id")
    val detailFlow = viewModel.detailFlow.collectAsState()
    if (id > 0) {
        viewModel.getDetail(id)
    }

    val state = rememberCollapsingToolbarScaffoldState()

    ZoomBoardTheme(darkTheme = false) {
        Surface(color = MaterialTheme.colors.background) {
            CollapsingToolbarScaffold(
                modifier = Modifier
                    .fillMaxSize(),
                state = state,
                scrollStrategy = ScrollStrategy.ExitUntilCollapsed,
                toolbar = {
                    Image(
                        modifier = Modifier
                            .background(OrangeMain)
                            .fillMaxWidth()
                            .height(240.dp)
                            .parallax(ratio = 0.2f)
                            .alpha(state.toolbarState.progress),
                        painter = painterResource(id = R.drawable.brunch_image),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                    )

                    Image(
                        modifier = Modifier
                            .pin()
                            .padding(16.dp)
                            .clickable { actions.navigateUp() },
                        painter = painterResource(id = R.drawable.abc_vector_test),
                        contentDescription = null
                    )
                }
            ) {
                LazyColumn(
                    Modifier
                        .fillMaxSize()
                        .padding(20.dp),
                ) {
                    item {
                        Text(
                            text = detailFlow.value.title,
                            color = Color.Black,
                            fontSize = 24.sp
                        )
                    }
                    item {
                        Text(
                            text = detailFlow.value.description,
                            color = descTextColor,
                            fontSize = 15.sp
                        )
                    }
                    item {
                        Text(
                            text = "8월 31일 오후 12시 시작",
                            color = Color.Black,
                            fontSize = 20.sp,
                            modifier = Modifier.padding(top = 30.dp)
                        )
                    }
                    item {
                        Text(text = "참여인원 ${detailFlow.value.maxUserCount}명",
                            color = Color.Black,
                            fontSize = 20.sp,
                            modifier = Modifier.padding(top = 10.dp)
                        )
                    }
                    item {
                        ConstraintLayout(
                            modifier = Modifier.padding(top = 10.dp)
                        ) {
                            val (profileImage, name, follower) = createRefs()
                            Image(
                                painter = painterResource(id = R.drawable.image_1),
                                modifier = Modifier
                                    .constrainAs(profileImage) {
                                        top.linkTo(parent.top)
                                    }
                                    .size(40.dp)
                                    .clip(RoundedCornerShape(25.dp)),
                                contentDescription = null
                            )
                            Text(
                                text = detailFlow.value.author.name,
                                modifier = Modifier.constrainAs(name) {
                                        top.linkTo(parent.top)
                                        start.linkTo(profileImage.end)
                                    }
                                    .padding(start = 10.dp),
                                color = Color.Black,
                                fontSize = 15.sp,
                            )
                            Text(
                                text = "122 followers",
                                modifier = Modifier.constrainAs(follower) {
                                    top.linkTo(name.bottom)
                                    start.linkTo(profileImage.end)
                                }
                                    .padding(start = 10.dp),
                                color = Color.Black,
                                fontSize = 15.sp,
                            )
                        }
                    }

                    item {
                        Text(text = "줌링크는 시작시간 10분전에 공개됩니다.",
                            color = Color.Black,
                            fontSize = 20.sp,
                            modifier = Modifier.padding(top = 20.dp)
                        )
                    }

                    item {
                        Button(modifier = Modifier
                            .padding(top = 30.dp)
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .background(color = OrangeMain, shape = RoundedCornerShape(15.dp)),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = OrangeMain,
                                contentColor = Color.Black
                            ),
                            onClick = {

                            }
                        ) {
                            Text(text = "참여하기",
                                modifier = Modifier.padding(15.dp),
                                color = Color.Black
                            )
                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun BoardDetailPreview() {
    val navController = rememberNavController()
    BoardDetailScreen(id = 0, actions = Actions(navController = navController))
}