package kr.co.cleaninglab.zoomboard.ui.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain
import kr.co.cleaninglab.zoomboard.ui.theme.textFieldBackground

@Composable
fun InputText(
    title: String,
    hintText: String,
    bottomMargin: Int = 26,
    singleLine: Boolean = true,
    enable: Boolean = true,
    inputValue: MutableState<TextFieldValue>
) {
    Column() {
        Text(text = title)
        TextField(
            value = inputValue.value,
            onValueChange = { t -> inputValue.value = t },
            placeholder = { Text(hintText) },
            modifier = Modifier
                .padding(top = 10.dp, bottom = bottomMargin.dp)
                .fillMaxWidth(),
            enabled = enable,
            singleLine = singleLine,
            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Black,
                backgroundColor = textFieldBackground,
                focusedIndicatorColor = Color.White,
                unfocusedIndicatorColor = Color.White,
                cursorColor = OrangeMain
            ),
            shape = RoundedCornerShape(10.dp)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun InputTextPreview() {
    val inputvalue = remember { mutableStateOf(TextFieldValue()) }
    InputText("클래스 제목",
        "같이 함덕 해수용장 봐요~",
        inputValue = inputvalue,
        singleLine = false
    )
}