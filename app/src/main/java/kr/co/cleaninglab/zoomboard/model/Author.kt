package kr.co.cleaninglab.zoomboard.model

import com.google.gson.annotations.SerializedName

data class Author(
    @SerializedName("name") val name: String,
    @SerializedName("profileUrl") val profileUrl: String?
)

fun emptyAuthor(): Author = Author("", "")