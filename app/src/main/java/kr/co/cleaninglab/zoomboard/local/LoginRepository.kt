package kr.co.cleaninglab.zoomboard.local

import com.eugene.logcatlib.Logcat
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kr.co.cleaninglab.zoomboard.model.*
import kr.co.cleaninglab.zoomboard.net.RemoteDataSource
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource
) {

    fun getToken(): Flow<String> {
        return localDataSource.getToken()
    }

    fun getName(): Flow<String> {
        return localDataSource.getName()
    }

    suspend fun createUser(googleId: String): String {
        return try {
            when (val result = remoteDataSource.login(googleToken = googleId)) {
                is ResultWrapper.Success -> {
                    localDataSource.setToken(result.value)
                    result.value
                }
                is ResultWrapper.Error -> {
                    Logcat.error(result.throwable)
                    ""
                }
                else -> ""
            }
        } finally {
        }
    }

    fun getProfile(): Flow<String?> = localDataSource.getName().map { name ->
        name.ifBlank {
            try {
                when (val result = remoteDataSource.getProfile()) {
                    is ResultWrapper.Success -> {
                        localDataSource.setName(result.value.name)
                        result.value.name
                    }
                    is ResultWrapper.Error -> {
                        Logcat.error(result.throwable)
                        null
                    }
                    else -> null
                }
            } finally {
            }
        }
    }

    suspend fun setName(profileRequest: ProfileRequest) {
        try {
            when (val result = remoteDataSource.setProfile(profileRequest)) {
                is ResultWrapper.Success -> {
                    localDataSource.setName(profileRequest.name)
                }
                is ResultWrapper.Error -> {
                    Logcat.error(result.throwable)
                }
            }
        } finally {
        }
    }
}