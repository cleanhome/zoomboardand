package kr.co.cleaninglab.zoomboard.ui.splash

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController
import com.eugene.logcatlib.Logcat
import kr.co.cleaninglab.zoomboard.R
import kr.co.cleaninglab.zoomboard.model.ProfileRequest
import kr.co.cleaninglab.zoomboard.ui.Actions
import kr.co.cleaninglab.zoomboard.ui.Destinations
import kr.co.cleaninglab.zoomboard.ui.common.InputText
import kr.co.cleaninglab.zoomboard.ui.common.MyTopAppBar
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain
import kr.co.cleaninglab.zoomboard.ui.theme.ZoomBoardTheme

object NextAction {
    const val Create = "create"
    const val Join = "join"
}

@Composable
fun NameScreen(
    viewModel: NameViewModel = hiltViewModel(),
    nextAction: String,
    actions: Actions?
) {
    Logcat.debug("--- nextAction : $nextAction")

    val nameState = viewModel.nameFlow.collectAsState()
    nameState.value?.let {
        if (it.isNotBlank()) {
            Logcat.debug("---- nameState! : $it")
            when (nextAction) {
                NextAction.Create -> {
                    actions?.navigateUp?.invoke()
                    actions?.goCreate?.invoke()
                }
            }
        }
    }

    viewModel.checkName()

    ZoomBoardTheme(darkTheme = false) {
        // A surface container using the 'background' color from the theme
        val nameValue = remember { mutableStateOf(TextFieldValue()) }

        Surface(color = MaterialTheme.colors.background) {
            Column(modifier = Modifier
                .fillMaxSize()) {

                MyTopAppBar(
                    title = "",
                    iconRes = R.drawable.ic_back_button,
                    iconVisible = true,
                    naviUpAction = actions!!.navigateUp
                )

                Column(modifier = Modifier.padding(20.dp)) {
                    InputText(title = "닉네임을 입력해주세요!",
                        hintText = "줌보드",
                        inputValue = nameValue)

                    Button(modifier = Modifier
                        .padding(0.dp)
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .background(color = OrangeMain, shape = RoundedCornerShape(10.dp)),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = OrangeMain,
                            contentColor = Color.Black
                        ),
                        onClick = {
                            viewModel.setName(ProfileRequest(nameValue.value.text))
                        }
                    ) {
                        Text(text = "이름입력",
                            modifier = Modifier.padding(15.dp),
                            color = Color.Black
                        )
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun NamePreview() {
    NameScreen(
        nextAction = "",
        actions = null
    )
}