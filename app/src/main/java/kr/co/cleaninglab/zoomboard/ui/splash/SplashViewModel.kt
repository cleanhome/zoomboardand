package kr.co.cleaninglab.zoomboard.ui.splash

import android.app.Activity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eugene.logcatlib.Logcat
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kr.co.cleaninglab.zoomboard.local.LoginRepository
import kr.co.cleaninglab.zoomboard.model.Login
import kr.co.cleaninglab.zoomboard.model.ZoomBoard
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val repository: LoginRepository
): ViewModel() {

    val tokenFlow: StateFlow<String> = repository.getToken().stateIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(),
        ""
    )

    val googleIdTokenFlow: StateFlow<String> = LoginManager.googleIdTokenFlow.stateIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(),
        ""
    )

    fun createUser(googleIdToken: String) {
        viewModelScope.launch {
            repository.createUser(googleIdToken)
        }
    }

    fun loginGoogle(activity: Activity) {
        viewModelScope.launch {
            LoginManager.initGoogleLogin(activity)
        }
    }
}