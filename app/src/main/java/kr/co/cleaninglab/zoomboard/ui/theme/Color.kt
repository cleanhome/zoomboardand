package kr.co.cleaninglab.zoomboard.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val YellowMain = Color(0xFFfff59d)
val YellowLight = Color(0xFFffffcf)
val YellowDark = Color(0xFFcbc26d)
val OrangeMain = Color(0xFFffa000)
val OrangeLight = Color(0xFFffd149)
val OrangeDark = Color(0xFFc67100)

val Sun = Color(0xFFef6c00)

val textFieldBackground = Color(0xFFE5E5E5)

val descTextColor = Color(0xff575757)