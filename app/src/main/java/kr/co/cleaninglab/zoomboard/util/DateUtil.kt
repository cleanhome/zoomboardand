package kr.co.cleaninglab.zoomboard.util

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.format(format: String): String {
    val simpleFormatter = SimpleDateFormat(format)
    return simpleFormatter.format(this)
}

fun getDate(year: Int, month: Int, day: Int): Date {
    val c = Calendar.getInstance()
    c.set(year, month, day)
    return c.time
}

fun getTime(hour: Int, minute: Int): Date {
    val c = Calendar.getInstance()
    c.set(currentYear(), currentMonth(), currentDay(), hour, minute)
    return c.time
}

fun currentYear(): Int {
    return Calendar.getInstance().get(Calendar.YEAR)
}

fun currentMonth(): Int {
    return Calendar.getInstance().get(Calendar.MONTH)
}

fun currentDay(): Int {
    return Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
}

fun currentHour(): Int {
    return Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
}

fun currentMinute(): Int {
    return Calendar.getInstance().get(Calendar.MINUTE)
}