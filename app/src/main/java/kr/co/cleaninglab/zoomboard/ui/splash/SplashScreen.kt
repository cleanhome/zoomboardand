package kr.co.cleaninglab.zoomboard.ui.splash

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.eugene.logcatlib.Logcat
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain
import kr.co.cleaninglab.zoomboard.ui.theme.ZoomBoardTheme

@Composable
fun SplashScreen(
    nameAction: (String) -> Unit,
    nextAction: (() -> Unit)?,
    viewModel: SplashViewModel = hiltViewModel()
) {
    val activity = LocalContext.current as Activity

    val googleIdTokenState = viewModel.googleIdTokenFlow.collectAsState()
    googleIdTokenState.value.let {
        Logcat.debug("---- token : $it")
        if (it.isNotBlank()) {
            viewModel.createUser(it)
        }
    }

    val tokenState = viewModel.tokenFlow.collectAsState()
    tokenState.value.let {
        Logcat.debug("---- zoomboard token : $it")
        if (it.isNotBlank()) {
            nameAction(NextAction.Create)
        }
    }

    ZoomBoardTheme {
        // A surface container using the 'background' color from the theme
        Surface(color = MaterialTheme.colors.background) {
            Column() {
                Button(onClick = { viewModel.loginGoogle(activity) },
                    modifier = Modifier
                        .padding(0.dp)
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .background(color = OrangeMain, shape = RoundedCornerShape(10.dp)),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = OrangeMain,
                        contentColor = Color.Black
                    )
                ) {
                    Text(
                        text = "google Login",
                        modifier = Modifier.padding(15.dp),
                    )
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ZoomBoardTheme {
        SplashScreen({}, null)
    }
}