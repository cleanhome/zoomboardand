package kr.co.cleaninglab.zoomboard.model

enum class ResultError {
    NETWORK,
    UN_KNOWN,
    LAZY,
    COMMON
}

sealed class ResultWrapper<out T> {
    data class Success<out T>(val value: T) : ResultWrapper<T>()
    data class Error(
        val type: ResultError,
        val throwable: Throwable? = null
    ) : ResultWrapper<Nothing>()

    object ResultNothing : ResultWrapper<Nothing>()
}