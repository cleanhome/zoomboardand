package kr.co.cleaninglab.zoomboard

import android.app.Application
import com.eugene.logcatlib.Logcat
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App: Application() {

    override fun onCreate() {
        super.onCreate()

        Logcat.initialize(true, "kr.co.cleaninglab")
    }
}