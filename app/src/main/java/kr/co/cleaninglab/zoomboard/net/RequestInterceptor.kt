package kr.co.cleaninglab.zoomboard.net

import kr.co.cleaninglab.zoomboard.BuildConfig
import kr.co.cleaninglab.zoomboard.local.SharedPreferences
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

const val AUTH_KEY = "auth-token"
const val OS_KEY = "os"
const val OS_VERSION_KEY = "os-version"
const val APP_VERSION_KEY = "app-version"
const val OS_VALUE = "android"
const val SETTING_REVISION = "setting-revision"
const val RESOLUTION = "resolution"

class RequestInterceptor(
    private val preferences: SharedPreferences
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestBuilder = originalRequest.newBuilder()
        val headers: Map<String, String> = mapOf(
            OS_KEY to "android",
            APP_VERSION_KEY to BuildConfig.VERSION_CODE.toString(),
            AUTH_KEY to preferences.tokenByBlocking
        )
        for (key in headers.keys) {
            val value = headers[key]
            requestBuilder.addHeader(key, value!!)
        }
        requestBuilder.method(originalRequest.method, originalRequest.body)
        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}