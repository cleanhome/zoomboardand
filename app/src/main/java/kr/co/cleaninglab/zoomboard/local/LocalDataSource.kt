package kr.co.cleaninglab.zoomboard.local

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {

    fun getToken(): Flow<String> {
        return sharedPreferences.getToken()
    }

    suspend fun setToken(token: String) {
        sharedPreferences.setToken(token)
    }

    fun getName(): Flow<String> {
        return sharedPreferences.getName()
    }

    suspend fun setName(name: String) {
        sharedPreferences.setName(name)
    }
}