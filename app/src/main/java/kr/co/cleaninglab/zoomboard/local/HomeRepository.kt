package kr.co.cleaninglab.zoomboard.local

import kotlinx.coroutines.flow.Flow
import kr.co.cleaninglab.zoomboard.net.RemoteDataSource
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) {

    fun getToken(): Flow<String> {
        return localDataSource.getToken()
    }

    fun getName(): Flow<String> {
        return localDataSource.getName()
    }
}