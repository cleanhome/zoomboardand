package kr.co.cleaninglab.zoomboard.model

data class CreateRequest(
    val title: String,
    val description: String,
    val maxCountUser: Int,
    val zoomLinkUrl: String
)
