package kr.co.cleaninglab.zoomboard.model

import com.google.gson.annotations.SerializedName


class Response<RESULT> {

    @SerializedName("code")
    var code: Int? = null

    @SerializedName("result")
    var result: RESULT? = null

    @SerializedName("msg")
    var msg: String? = null
}