package kr.co.cleaninglab.zoomboard.model

import com.google.gson.annotations.SerializedName

data class ZoomBoard(
    @SerializedName("id") val id: Int,
    @SerializedName("author") val author: Author,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("maxUserCount") val maxUserCount: Int,
)

fun emptyBoard() = ZoomBoard(0, emptyAuthor(), "", "", 0)