package kr.co.cleaninglab.zoomboard.ui.home

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun MenuScreen() {
    Column() {
        Text(text = "Menu")
    }
}