package kr.co.cleaninglab.zoomboard.ui.common

import androidx.annotation.DrawableRes
import androidx.compose.foundation.clickable
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.eugene.logcatlib.Logcat
import kr.co.cleaninglab.zoomboard.R
import kr.co.cleaninglab.zoomboard.ui.theme.OrangeMain

@Composable
fun MyTopAppBar(
    title: String,
    iconVisible: Boolean,
    @DrawableRes iconRes: Int = R.drawable.ic_plus,
    naviUpAction: () -> Unit = {}
) {
    TopAppBar(title = { Text(text = title, fontSize = 15.sp) },
        backgroundColor = OrangeMain,
        navigationIcon = {
            if (iconVisible) {
                Icon(
                    painter = painterResource(id = iconRes),
                    null,
                    modifier = Modifier.clickable(onClick = {
                        Logcat.debug("-- click menu")
                        naviUpAction()
                    })
                )
            }
        })
}

@Preview(showBackground = true)
@Composable
fun HomePreview() {
    MyTopAppBar("Zoom Board", iconVisible = false)
}