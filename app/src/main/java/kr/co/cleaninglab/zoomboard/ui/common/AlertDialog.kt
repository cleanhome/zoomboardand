package kr.co.cleaninglab.zoomboard.ui.common

import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import kr.co.cleaninglab.zoomboard.ui.theme.textFieldBackground

@Composable
fun CommonDialog(
    title: String,
    description: String? = null,
    confirmClick: () -> Unit,
    cancelClick: () -> Unit
) {
    AlertDialog(
        onDismissRequest = {},
        title = {
            Text(text = title)
        },
        text = {
            description?.let {
                Text(
                    text = description
                )
            }
        },
        confirmButton = {
            Button(
                onClick = {
                    confirmClick()
                }) {
                Text("OK", color = Color.Black)
            }
        },
        dismissButton = {
            Button(
                onClick = {
                    cancelClick()
                }) {
                Text("cancel", color = textFieldBackground)
            }
        }
    )

}
