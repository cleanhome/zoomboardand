package kr.co.cleaninglab.zoomboard.local

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import androidx.annotation.ColorInt
import com.eugene.logcatlib.Logcat
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kr.co.cleaninglab.zoomboard.model.CreateRequest
import kr.co.cleaninglab.zoomboard.model.ResultWrapper
import kr.co.cleaninglab.zoomboard.net.RemoteDataSource
import java.io.File
import java.io.IOException
import java.io.OutputStream
import javax.inject.Inject

class CreateRepository @Inject constructor(
    @ApplicationContext val context: Context,
    val remoteDataSource: RemoteDataSource
) {

    suspend fun create(createRequest: CreateRequest): String {
        return try {
            when (val result = remoteDataSource.create(createRequest)) {
                is ResultWrapper.Success -> result.value
                is ResultWrapper.Error -> {
                    Logcat.error(result.throwable)
                    ""
                }
                else -> ""
            }
        } catch (e: Exception) {
            Logcat.error(e)
            ""
        }
    }

    suspend fun putBitmap(bitmap: Bitmap): String {
        return try {
            val filePath = toFile(context, bitmap)
            when (val result = remoteDataSource.putBitmap(File(filePath))) {
                is ResultWrapper.Success -> result.value
                is ResultWrapper.Error -> {
                    Logcat.error(result.throwable)
                    ""
                }
                else -> ""
            }
        } catch (e: Exception) {
            Logcat.error(e)
            ""
        }
    }

    private suspend fun toFile(context: Context, bitmap: Bitmap): String = withContext(
        Dispatchers.IO
    ) {
        val fileName = "sign_${System.currentTimeMillis()}.jpg"
        Logcat.debug("---- write to file : ${context.filesDir.absolutePath} file : $fileName")
        context.openFileOutput(fileName, Context.MODE_APPEND).use {
            saveBitmapToFile(bitmap, it, Bitmap.CompressFormat.JPEG)
        }
        "${context.filesDir.absolutePath}/$fileName"
    }
}

@Throws(IOException::class)
fun saveBitmapToFile(
    bitmap: Bitmap,
    stream: OutputStream,
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG,
    @ColorInt backgroundColor: Int = Color.WHITE
) {
    val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(newBitmap)
    canvas.drawColor(backgroundColor)
    canvas.drawBitmap(bitmap, 0f, 0f, null)
    newBitmap.compress(format, 80, stream)
    stream.close()
    bitmap.recycle()
}