package kr.co.cleaninglab.zoomboard.model

import java.lang.RuntimeException

data class ResponseException(
    val code: Int?,
    val msg: String?,
    val data: String?
): RuntimeException(msg) {
    companion object {
        fun of(response: Response<*>): ResponseException {
            return ResponseException(response.code, response.msg, null)
        }
    }
}
