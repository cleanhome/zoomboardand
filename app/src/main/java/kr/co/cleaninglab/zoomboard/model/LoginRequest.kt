package kr.co.cleaninglab.zoomboard.model

data class LoginRequest(
    val loginType: String,
    val token: String
)
