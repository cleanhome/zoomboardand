package kr.co.cleaninglab.zoomboard.ui.create

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eugene.logcatlib.Logcat
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kr.co.cleaninglab.zoomboard.local.CreateRepository
import kr.co.cleaninglab.zoomboard.model.CreateRequest
import javax.inject.Inject

@HiltViewModel
class CreateViewModel @Inject constructor(
    private val repository: CreateRepository
): ViewModel() {

    val imagePathFlow = MutableStateFlow("")

    val createCompleteFlow = MutableStateFlow<String>("")

    fun putBitmap(bitmap: Bitmap) {
        viewModelScope.launch {
            imagePathFlow.value = repository.putBitmap(bitmap = bitmap)
            Logcat.debug("imagePath : ${imagePathFlow.value}")
        }
    }

    fun create(createRequest: CreateRequest) {
        viewModelScope.launch {
            createCompleteFlow.value = repository.create(createRequest = createRequest)
        }
    }
}