package kr.co.cleaninglab.zoomboard.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kr.co.cleaninglab.zoomboard.local.LoginRepository
import kr.co.cleaninglab.zoomboard.model.ProfileRequest
import javax.inject.Inject

@HiltViewModel
class NameViewModel @Inject constructor(
    private val repository: LoginRepository
) : ViewModel() {

    var nameFlow: MutableStateFlow<String?> = MutableStateFlow(null)

    fun checkName() {
        viewModelScope.launch {
            repository.getProfile().collect { name ->
                name?.let {
                    nameFlow.value = it
                }
            }
        }
    }

    fun setName(profileRequest: ProfileRequest) {
        viewModelScope.launch {
            repository.setName(profileRequest)
        }
    }
}